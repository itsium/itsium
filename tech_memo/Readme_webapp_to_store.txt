1. Build Android apk

cordova build --release android
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore certificates/gplay.jks platforms/android/ant-build/Yuemu-release-unsigned.apk gplay
jarsigner -verify -verbose -certs platforms/android/ant-build/Yuemu-release-unsigned.apk
zipalign -v 4 platforms/android/ant-build/Yuemu-release-unsigned.apk Yuemu-latest.apk
