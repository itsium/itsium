Before run bacula backup add a crontab backup to backup database in /data/backup

1. Install

sudo apt-get update
sudo apt-get install bacula-client

2. Edit Backup Server Cong 

sudo emacs /etc/bacula/bacula-dir.conf

add a new Client ->

Client {
  Name = hostname.domain-fd # modify this
  Address = hostname.domain # modify this
  FDPort = 9102
  Catalog = MyCatalog
  Password = "REMOTEFD_Cki04vyLTLg1WsvmzF1W4mwrIx-pSmtDX"          # password for Remote FileDaemon                           
  File Retention = 30 days            # 30 days                                                                               
  Job Retention = 6 months            # six months                                                                            
  AutoPrune = yes                     # Prune expired Jobs/Files                                                              
}

add a backup job ->

Job {
  Name = NewServRemoteBackup  # modify this
  JobDefs = DefaultJob
  Client = hostname.domain-fd  #modify this
  Pool = RemoteFile
  Type = Backup
  FileSet = "Full Set"
  Schedule = WeeklyCycle
  Storage = File
  Messages = Standard
}

add a restore job -> 

Job {
  Name = "RestoreHapps" # modify this
  Type = Restore
  Client=happs.zhanshengjie.com-fd  #modify this
  FileSet="Full Set"
  Storage = File
  Pool = Default
  Messages = Standard
  Where = /mybackup/restore
}


3. Edit REMOTE Conf

sudo emacs /etc/bacula/bacula-fd.conf

add -> 

Director {
  Name = vulpix-dir
  Password = "REMOTEFD_Cki04vyLTLg1WsvmzF1W4mwrIx-pSmtDX"
}

Director {
  Name = vulpix-mon
  Password = "MONITOR_YvjgEPkDd66GYemEEOKOvdsIoNbfLdvEm"
  Monitor = yes
}

FileDaemon {                          
  Name = happs.zhanshengjie.com-fd  #modify this
  FDport = 9102                  
  WorkingDirectory = /var/lib/bacula
  Pid Directory = /var/run/bacula
  Maximum Concurrent Jobs = 20
  FDAddress = 0.0.0.0
}


Messages {
  Name = Standard
  director = vulpix-dir = all, !skipped, !restored
}


4. Make sure the port is open on the remote server  with -> 

sudo ufw status

if not add ->

sudo ufw allow 9102

5. launch remote client

sudo bacula-fd -tc /etc/bacula/bacula-fd.conf
sudo service bacula-fd restart

6. test on backup server

https://192.168.2.88:10000/



